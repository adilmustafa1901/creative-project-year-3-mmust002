using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PickUps : MonoBehaviour
{

    //script is used to deal with collisions

    private Player player; //used to detect if the player is over an object/collectable
    public int CollectableVal; //used to provide each collectable with a unique score
    public int CollectableType; // -1 = end of level, 0 = exit/not collectabe, 1 = KeyCard, 2 = normal pickup, 3 = bonus pickup

    public TextMeshProUGUI KeyCardFoundText;

    void Start()
    {
        //gains access to the Player Script's variables
        player = FindObjectOfType<Player>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //keycard collision detection 
        if (collision.CompareTag("PlayerT") && CollectableType == 1)
        {
            Destroy(gameObject);
            player.KeyCardManager(CollectableVal,true);
        }

        //normal collectable collision detection
        if (collision.CompareTag("PlayerT") && CollectableType == 2)
        {
            Destroy(gameObject);
            player.CollectablesManager(CollectableVal);
        }

        //exit collision detection
        if (collision.CompareTag("PlayerT") && CollectableType == 0 && KeyCardFoundText.text == "Objective: Escape!")
        {
            //used for testing purposes
            //Destroy(gameObject);
        }

        //End of Level collision detection
        if (collision.CompareTag("PlayerT") && CollectableType == -1)
        {
            //used for testing purposes
            Destroy(gameObject);
            player.EndTutorial(CollectableVal);
        }

        if (collision.CompareTag("PlayerT") && CollectableType == -10)
        {
            //used for testing purposes
            Destroy(gameObject);
            player.EndLevel(CollectableVal);
        }

        //Enterance collision detection
        if (collision.CompareTag("PlayerT") && CollectableType == -2)
        {
            //used for testing purposes
            //Destroy(gameObject);

        }
    }
}
