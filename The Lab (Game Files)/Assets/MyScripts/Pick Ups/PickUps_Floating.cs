using System.Collections;
using System;
using UnityEngine;

public class PickUps_Floating : MonoBehaviour
{

    float posY;
    public float Distance = 1;

    //https://gamedev.stackexchange.com/questions/96878/how-to-animate-objects-with-bobbing-up-and-down-motion-in-unity
    //the link above provides information on how someone would make a 3D object float up and down, ive applied it to a 2D environment

    // Start is called before the first frame update
    void Start()
    {
        this.posY = this.transform.position.y; //selects starting position 
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector2(transform.position.x,
            posY + ((float)Math.Sin(Time.time * 2) * Distance));  //allows the game object to float on its Y axis
    }
}
