using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPackScript : MonoBehaviour
{
    //script is used to create a health pack 

    private Player player; //Player script
    public int HealthIncrease; //healing value
    private float playerHealth; //players health 


    // Start is called before the first frame update
    void Start()
    {
        //access to the player script variables
        player = FindObjectOfType<Player>();
        HealthIncrease = 150;
    }

    //if the player enters the health pack
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerT"))
        {
            player.HealthManager(HealthIncrease); //health is increased (if less than 255)
            Destroy(gameObject); //destroys game object 
            Debug.Log(playerHealth); //console says the players health 
        }
    }

    //if the player stays in the health pack
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerT"))
        {
            player.HealthManager(HealthIncrease); //health is increased (if less than 255)
            Destroy(gameObject); //destroys game object 
            Debug.Log(playerHealth); //console says the players health 
        }
    }
}
