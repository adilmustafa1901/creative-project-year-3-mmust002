using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps_DestroyAllClones : MonoBehaviour
{
    //this script is for a pickup that can destroy clones (was used for testing)

    private Player player; //Player Script
    public DestroyClones destroyClones; //DestroyClones script

    void Start()
    {
        //gains access to their variables 
        player = FindObjectOfType<Player>();
        destroyClones = GetComponent<DestroyClones>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //keycard collision detection 
        if (collision.CompareTag("PlayerT"))
        {
            destroyClones.TriggerCountdown = true; //triggers coutdown
            Destroy(gameObject); //destroys the game object 
        }
    }

    
}