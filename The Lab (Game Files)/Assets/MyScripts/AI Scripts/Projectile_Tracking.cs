using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Tracking : MonoBehaviour
{
    //this script is used to alter projectiles

    public float speed; //alters the speed of the projectile

    private Transform player; //stores players position
    private Vector2 target; //stores the portition of the player when the projectile was created
    private int projectileLife; // counter that increases over time
    private int projectileLife_Modulo; // length of how long it takes for projectile to despawn

    public int projectileLife_multiplier = 2; //converts projectile life into seconds
    public bool Destroy_When_At_Target_Position; //projectile despawns when at target location 
    public bool ProvideProjectileHealth = true; //enables projectile life timer
    public bool EnableTracking = true; //used for projectiles I dont want to have A* pathfinding 
    private bool EnableDestroy = true; //allows the projectile to be destroyed
    public bool EnableCollisionDestroy = true; //allows the projectile to be destroyed when collission occurs
    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("PlayerT").transform;
        target = new Vector2(player.position.x, player.position.y);
    }

    // Update is called once per frame
    void Update()
    {

        if (EnableTracking)
        {
            //travels to the the players position (doesnt track them completly)
            transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        }

        //sets the modulo (60 is the frame rate & multiplier is essentially seconds)
        projectileLife_Modulo = 60 * projectileLife_multiplier;

        //applies modulo to projectile life so it can reset to 0 
        projectileLife = projectileLife % projectileLife_Modulo;

        //keeps adding 1 to projectile life
        projectileLife++;

        //used for testing purposes
        //Debug.Log(projectileLife);

        if (transform.position.x > 50 && transform.position.y > 25)
        {
            EnableDestroy = false;
        }
        else EnableDestroy = true;

        if (Destroy_When_At_Target_Position)
        {
            //destroys the projectile when it reaches its target position 
            if (transform.position.x == target.x && transform.position.y == target.y)
            {
                Destroy(gameObject);
                Debug.Log("Projectile Reached Target");
            }
        }

        if (ProvideProjectileHealth)
        {
            //destroys projectile when projectileLife = projectileLife_modulo
            if (projectileLife == projectileLife_Modulo && EnableDestroy)
            {
                Destroy(gameObject);
                Debug.Log("Projectile Reached Life Modulo");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        //if it collides with the player destroy game object
        if (collision.CompareTag("PlayerT") && EnableCollisionDestroy)
        {
            Destroy(gameObject);
        }
    }
}
