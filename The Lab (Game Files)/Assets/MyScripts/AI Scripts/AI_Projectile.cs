using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Projectile : MonoBehaviour
{

    //this tutorial  https://www.youtube.com/watch?v=_Z1t7MNk0c4&list=PLBIb_auVtBwDgHLhYc-NG633rTbTPim9z&index=3

    public float speed; //controls the speed of the AI 

    public float stoppingDistance; //decides stopping distance
    public float retreatDistance; //decides retreat distance

    private float timeBetweenShots; //decides the rate of fire
    public float startTimeBetweenShots; //decides when to being shooting

    public bool Enable = true, EnableProjectileParent = false; //enables projectiles & stores them in a gameobject

    //private int projectileLife;
    //private int projectileLife_Modulo;
    //public int projectileLife_multiplier = 2;

    public Transform player, ProjectileParent; //used to calculate distance
    public GameObject projectile; //used to store the projectile

    //private Projectile_Tracking PT;

    public bool StalkPlayer; //if true the AI wouldnt use A* pathfinding it will instead slowly follow them 


    // Start is called before the first frame update
    void Start()
    {
        //links players position to variable
        player = GameObject.FindGameObjectWithTag("PlayerT").transform;

        //rate of fire (e.g 1 = 1 second)
        timeBetweenShots = startTimeBetweenShots;

        //GameObject g = GameObject.FindGameObjectWithTag("AI");
        //PT = g.GetComponent<Projectile_Tracking>();
        //PT.EnableDestroy = true;

        //PT.EnableDestroy = true;

        //sets the modulo (60 is the frame rate & multiplier is essentially seconds)
        //projectileLife_Modulo = 60 * projectileLife_multiplier;

        //applies modulo to projectile life so it can reset to 0 
        //projectileLife = projectileLife % projectileLife_Modulo;

        //keeps adding 1 to projectile life
        //projectileLife++;
    }

    // Update is called once per frame
    void Update()
    {
        //if enabled the AI stalks the user if they are near them 
        if (StalkPlayer)
        {
            if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            }

            else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
            {
                transform.position = this.transform.position;
            }

            else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
            }
        }

        if (Enable)
        {
            //shoots projectile when = 0 or less
            if (timeBetweenShots <= 0)
            {
                //creates projectile
                if (EnableProjectileParent)
                {
                    Instantiate(projectile, transform.position, Quaternion.identity).transform.parent = ProjectileParent;
                    projectile.transform.parent = ProjectileParent;
                }
                //if(EnableProjectileParent)
                //projectile.transform.parent = ProjectileParent;
                
                //restarts the variable 
                timeBetweenShots = startTimeBetweenShots;
            }
            else
            {
                //variable counts down until = 0 or less
                timeBetweenShots -= Time.deltaTime;
            }
        }

        

    }
}
