using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patroling_AI : MonoBehaviour
{
    // Start is called before the first frame update
    //https://www.youtube.com/watch?v=8eWbSN2T8TE&t=3s This tutorial provided the code in this script

    //used to alter the speed of the AI 
    public float speed;
    public Transform[] moveSpots; //used to store all the points/locations the AI will travel to

    private int randomSpot; //selects a random spot

    private float waitTime; //wait time when at a spot
    public float startWaitTime; //to make sure the wait time restarts to the original time 

    void Start()
    {
        waitTime = startWaitTime; //links startWaitTime to waitTime
        randomSpot = Random.Range(0, moveSpots.Length); //selects a random spot
    }

    // Update is called once per frame
    void Update()
    {
        //selects a random spot, makes AI move towards the spot
        transform.position = Vector2.MoveTowards(transform.position,moveSpots[randomSpot].position, speed * Time.deltaTime);

        //if the distance between the AI and the move spot is less than 0.2
        if(Vector2.Distance(transform.position, moveSpots[randomSpot].position) < 0.2f)
        {
            //if wait time is less than or equal to 0
            if(waitTime <= 0)
            {
                //selects a new spot
                randomSpot = Random.Range(0, moveSpots.Length);

                //restarts the wait time
                waitTime = startWaitTime;
            } 

            //if the distance between the AI and the move spot is more than 0.2
            else
            {
                //make wait time decrease every second
                waitTime -= Time.deltaTime;
            }
        }

    }
}
