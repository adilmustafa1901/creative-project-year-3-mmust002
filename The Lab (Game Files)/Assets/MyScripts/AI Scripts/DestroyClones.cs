using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyClones : MonoBehaviour
{
    //script is used to destroy all clones in the first segment in level 3

    private Player player; //stores Player script
    public GameObject CloneParent, TnT; //game objects used for the mechanics

    public float CountdownTimer; //used to store the countdown timer
    public float StartTime = 12; //used to store the start time
    public bool TriggerCountdown; //used to trigger the countdown


    // Start is called before the first frame update
    void Start()
    {
        //gains access to the player script variables 
        player = FindObjectOfType<Player>();

        //links the countdown timer to the start time
        CountdownTimer = StartTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (TriggerCountdown)
        {
            //Countdown
            if (CountdownTimer <= 0)
            {
                //makes sure it doesnt go less than 0
                CountdownTimer = 0;
            }
            else
            {
                //constantly decreases the countdown timer
                CountdownTimer -= Time.deltaTime;
            }
        }

        //when the timer hits 0 the CloneParent gets destroyed
        if(CountdownTimer == 0)
        {
            Destroy(CloneParent);
        }

        //if the TnT game object gone trigger the countdown
        if (TnT == null)
        {
            TriggerCountdown = true;
        }
    }
}
