using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhantomProjectile : MonoBehaviour
{

    //this tutorial showed me how to shoot projectiles https://www.youtube.com/watch?v=_Z1t7MNk0c4&list=PLBIb_auVtBwDgHLhYc-NG633rTbTPim9z&index=2


    //this script is mainly used to choose what projectile the phantom uses 

    public float speed; //Phantom speed
    public float stoppingDistance; //stopping distance
    public float retreatDistance; //retreat distance

    private float timeBetweenShots; //controls the speed the phantom shoots
    public float startTimeBetweenShots; //how long it takes to first shoot

    public bool Enable = true, EnableProjectileParent = false; //enables projectiles & stores them in a game object 

    public Transform player, ProjectileParent; //stores player and projectile parent location 
    public GameObject[] projectile; //stores multiple projectiles
    private int projectileSize; //stores how many projectiles are stored
    public int projectileSelected = 0; //selects the projectile chosen



    // Start is called before the first frame update
    void Start()
    {
        //links players position to variable
        player = GameObject.FindGameObjectWithTag("PlayerT").transform;

        //rate of fire (e.g 1 = 1 second)
        timeBetweenShots = startTimeBetweenShots;

        //links projectile size to the size of the projectile array 
        projectileSize = projectile.Length;

    }

    // Update is called once per frame
    void Update()
    {
        //projectile selected increments constantly while being modulo by the projectileSize
        projectileSelected = projectileSelected % projectileSize;
        projectileSelected++;

        if (Enable)
        {
            //shoots projectile when = 0 or less
            if (timeBetweenShots <= 0)
            {
                //creates projectile
                if (EnableProjectileParent)
                {
                    Instantiate(projectile[-1 + projectileSelected], transform.position, Quaternion.identity).transform.parent = ProjectileParent;
                    projectile[-1 + projectileSelected].transform.parent = ProjectileParent;
                }

                //restarts the variable 
                timeBetweenShots = startTimeBetweenShots;
            }
            else
            {
                //variable counts down until = 0 or less
                timeBetweenShots -= Time.deltaTime;
            }
        }



    }
}
