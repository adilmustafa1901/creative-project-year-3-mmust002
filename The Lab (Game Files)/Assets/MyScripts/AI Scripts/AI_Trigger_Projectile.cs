using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Trigger_Projectile : MonoBehaviour
{
    //this script is for the trap room in level 3 

    [SerializeField] private Animator Animation1; //stores the animation 
    public AI_Projectile AI; //stores the AI_Projectile script 

    // Start is called before the first frame update
    void Start()
    {
        //allows access to its variables
        AI.GetComponent<AI_Projectile>();
    }

    // Update is called once per frame
    void Update()
    {
        //if the animation is true, the AI is able to shoot projectiles
        if(Animation1.GetBool("Trap2") == true)
        {
            AI.Enable = true;
        }
    }
}
