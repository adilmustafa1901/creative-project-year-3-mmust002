using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class AI_Features : MonoBehaviour
{


    /* This tutorial helped me create this script https://www.youtube.com/watch?v=jvtFUfJ6CP8*/

    
    public Transform target; //allows the AI to chase a target
    public float speed = 200f; //provides speed
    public float nextWaypointDistance = 3f; //provides the next waypoint distance (default value)

    Path path; //uses A* pathfinding
    int currentWaypoint = 0; //sets its default value
    bool reachedEndofPath = false; //becomes true if the AI reaches the end of its path

    Seeker seeker; //allows script to interact with seeker component
    Rigidbody2D rb; //allows script to interact with rigidbody2D component

    // Start is called before the first frame update
    void Start()
    {
        seeker = GetComponent<Seeker>(); //assigns seeker variable to seeker component
        rb = GetComponent<Rigidbody2D>(); //assigns rb variable with rigidbody2D component

        InvokeRepeating("UpdatePath",0f,.5f); //calls UpdatePath() twice a second
    }

    void UpdatePath()
    {
        //if the AI has reached its destination it resets its path
        if(seeker.IsDone())
        seeker.StartPath(rb.position, target.position, OnPathComplete);
    }

    void OnPathComplete(Path p)
    {
        //if there isnt an error path it resets the current waypoint
        if(!p.error)
        {
            path = p;
            currentWaypoint = 0; 
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if null return nothing
        if(path == null)
            return;

        //if the current waypoint is more than or equal to the pathfinding count reachedEndofPath = true
        if(currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndofPath = true;
            return;
        }
        //if its not more than it will equal false
        else
        {
            reachedEndofPath = false;
        }

        //normalises the direction
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        
        //applies force to the AI so it flows better
        Vector2 force = direction * speed * Time.deltaTime;

        //applies the force to the rigidbody2D
        rb.AddForce(force);

        //determines the distance
        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        //if distance is less than the nextWaypointDistance currentWaypoint +1
        if(distance < nextWaypointDistance)
        {
            currentWaypoint ++;
        }
    }
}
