using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public ChaseState chaseState; //used to switch to the ChaseState
    public bool CanSeeThePlayer; //true when player is in range
    public bool enablePF; //enables pathfinding
    public Pathfinding_enabler PFE; //uses pathfinding_enabler script
    public float ViewingDistance = 15f, Dist; //used to store distance
    public Transform target, AI_Pos; //used to calculate disance between the player and AI

    //used to switch state
    public override State RunCurrentState()
    {
        //if the player is within range switch to the chase state
        if (CanSeeThePlayer)
        {
            return chaseState;
        }
        else
        {
            return this;
        }
    }

    void Start()
    {
        //used to gain access to the scripts variables
        PFE.GetComponent<Pathfinding_enabler>();
        AI_Pos.GetComponent<AI_Position>();
    }

    void Update()
    {
        //if player is in range enable pathfinding
        if (CanSeeThePlayer)
        {
            enablePF = true;
            PFE.enablePathfinding = true;
        }
        else //disable pathfinding 
        {
            enablePF = false;
            PFE.enablePathfinding = false;
        }

        //calculates distance between player and AI 
        if (target)
        {
            Dist = Vector2.Distance(target.position, AI_Pos.position);
        }

        //if player is less than viewingDistance CanSeeThePlayer = true
        if(Dist < ViewingDistance)
        {
            CanSeeThePlayer = true;
        }
        //if player is more than viewingDistance CanSeeThePlayer = false
        else
        {
            CanSeeThePlayer = false;
        }
    }
}
