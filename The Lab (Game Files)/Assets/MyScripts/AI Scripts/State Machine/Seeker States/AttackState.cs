using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class AttackState : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public DisbleGameObject disbleGameObject; //used to disable the AI (in level 3 player can destroy the seekers)
    public IdleState idleState; //switches to the idle state
    public ChaseState chaseState; //switches to the chase state
    public bool IsNotInAttackRange, FarFromPlayer; //checks the range of the player
    public float Dist; //stores distance
    public TextMeshPro text; //displays state


    public Transform target, AI_Pos; //used to calculate distance 

    //used to switch state
    public override State RunCurrentState()
    {
        //switches to chase state
        if(IsNotInAttackRange)
        {
            return chaseState;
        }
        //switches to idle state
        else if (FarFromPlayer)
        {
            return idleState;
        }
        //returns this state
        else
        {
            return this;
        }
    }

    private void Start()
    {
        //allows this script's variables to be used
        disbleGameObject.GetComponent<DisbleGameObject>();
    }

    private void Update()
    {
        //calculates the distance between the player and AI 
        if (target)
        {
            Dist = Vector2.Distance(target.position, AI_Pos.position);
        }

        //if true IsNotInAttackRange = true;
        if (Dist > 2f && Dist < 15f)
        {
            IsNotInAttackRange = true;
        }
        //if false IsNotInAttackRange = false;
        else
        {
            IsNotInAttackRange = false;
        }

        //if true FarFromPlayer = true;
        if (Dist > 15)
        {
            FarFromPlayer = true;
        }
        //if false FarFromPlayer = false;
        else
        {
            FarFromPlayer = false;
        }

        //changes state text
        if (FarFromPlayer) text.text = "Idle";
        else if (IsNotInAttackRange) text.text = "Chase";
        else if (!IsNotInAttackRange) text.text = "Slowing down";
    }
}

