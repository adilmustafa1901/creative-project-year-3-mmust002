using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class ChaseState : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public AttackState attackState; //used to switch to the attack state
    public IdleState idleState; //used to switch to the idle state
    public bool IsInAttackRange, FarFromPlayer; //used to see if the player is too far or close to the AI 
    public AIPath aIPath; //used to alter the A* pathfinding AIPath script
    public float AttackRangeVal = 1f, SprintVal = 5f, ChaseVal; 
    public int ChaseSpeed = 5, SprintSpeed = 10; //provides different speeds to the AI 
    public Transform target, AI_Pos; //used to calculate distance

    public override State RunCurrentState()
    {
        //if in attack range switch to the attack state
        if (IsInAttackRange)
        {
            return attackState;
        }
        //if the player is far from the AI switch to the idle state
        else if (FarFromPlayer)
        {
            return idleState;
        }
        //return this state
        else
        {
            return this;
        }
    }

    private void Start()
    {
        //used to alter the variables within these scripts
        aIPath.GetComponent<AIPath>();
        idleState.GetComponent<IdleState>();

        //links ChaseVal to the idleState's viewing distance variable
        ChaseVal = idleState.ViewingDistance;
    }

    private void Update()
    {
        //checks if the AI is in attacking range
        if (idleState.Dist < AttackRangeVal)
        {
            IsInAttackRange = true;
            FarFromPlayer = false;
        }
        //used to make the AI faster
        if (idleState.Dist > AttackRangeVal && idleState.Dist < SprintVal)
        {
            aIPath.maxSpeed = SprintSpeed;
            IsInAttackRange = false;
            FarFromPlayer = false;
        }
        //used to provide the AI with its normal chase speed
        if (idleState.Dist > SprintVal && idleState.Dist < ChaseVal)
        {
            aIPath.maxSpeed = ChaseSpeed;
            IsInAttackRange = false;
            FarFromPlayer = false;
        }
        //used to make AI idle
        if(idleState.Dist > idleState.ViewingDistance)
        {
            FarFromPlayer = true;
            IsInAttackRange = false;
        }
    }
}
