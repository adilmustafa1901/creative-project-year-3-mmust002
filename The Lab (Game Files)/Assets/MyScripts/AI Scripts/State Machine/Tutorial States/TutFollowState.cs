using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutFollowState : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s
    public Transform AI, player; //used to makw the AI go towards the players position
    public float speed; //determines the speed of the AI 
    public TutIdleState idleState; //used so the statemachine can return to idle

    public override State RunCurrentState()
    {
        //if PlayerIsInRange from the idle state script is equal to false return the idleState
        if (idleState.PlayerIsInRange == false)
        {
            return idleState;
        }
        //if PlayerIsInRange from the idle state script is equal to true return this state
        else
        {
            return this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //used to check/inherit the variables from the idleState script
        idleState.GetComponent<TutIdleState>();
    }

    // Update is called once per frame
    void Update()
    {
        //if PlayerIsInRange from the idle state script is equal to true
        if (idleState.PlayerIsInRange)
        {
            //Update the AIs position to move towards the position of the player
            AI.position = Vector2.MoveTowards(AI.position, player.position, speed * Time.deltaTime);
        }
    }
}
