using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutIdleState : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public bool PlayerIsInRange; //will equal true when the player is in range
    public Transform AI, player; //used to track the distance between the AI and the player
    public float Dist, customRange = 2f; //Dist is used to store the distance between the AI and player, customRange is used to switch state
    public TutFollowState followState; //the state that the state manager will switch to when PlayerInRange = true
    public TextMeshPro StateText;

    public override State RunCurrentState()
    {
        //if PlayerIsInRange equals true
        if (PlayerIsInRange == true)
        {
            //state manager will use the follow state as its 'currentState'
            return followState;
        }
        else
        {
            //if PlayerIsInRange = false the currentState inside the state manager will be this one (idleState)
            return this;
        }
    }


    // Update is called once per frame
    void Update()
    {
        //if player exists
        if (player)
        {
            //checks the distance between the player and the AI 
            Dist = Vector2.Distance(player.position, AI.position);
        }

        //if Dist is less than the customRange
        if (Dist < customRange)
        {
            //make PlayerIsInRange = true
            PlayerIsInRange = true;
        }

        //if Dist is more than the customRange
        if (Dist > customRange)
        {
            //make PlayerIsInRange = false 
            PlayerIsInRange = false;
        }

        //if player is in range change the text to "Follow"
        if (PlayerIsInRange)
        {
            StateText.text = "Follow";
        }

        //if player is not in range change the text to "Idle"
        else if (!PlayerIsInRange)
        {
            StateText.text = "Idle";
        }
    }
}
