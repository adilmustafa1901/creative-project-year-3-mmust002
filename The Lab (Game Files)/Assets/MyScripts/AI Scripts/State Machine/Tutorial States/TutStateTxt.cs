using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutStateTxt : MonoBehaviour
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public bool PlayerIsInRange; //will equal true when the player is in range
    public Transform AI, player; //used to track the distance between the AI and the player
    public float Dist, customRange = 2f; //Dist is used to store the distance between the AI and player, customRange is used to switch state
    public TextMeshPro StateText;

    // Update is called once per frame
    void Update()
    {
        //if player exists
        if (player)
        {
            //checks the distance between the player and the AI 
            Dist = Vector2.Distance(player.position, AI.position);
        }

        //if Dist is less than the customRange
        if (Dist < customRange)
        {
            //make PlayerIsInRange = true
            PlayerIsInRange = true;
        }

        //if Dist is more than the customRange
        if (Dist > customRange)
        {
            //make PlayerIsInRange = false 
            PlayerIsInRange = false;
        }

        //changes the text to attack when in range
        if (PlayerIsInRange)
        {
            StateText.text = "Attack";
        }

        //changes the text to Idle when not in range
        else if (!PlayerIsInRange)
        {
            StateText.text = "Idle";
        }
    }
}
