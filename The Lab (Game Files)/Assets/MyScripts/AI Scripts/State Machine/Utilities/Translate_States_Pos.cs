using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Translate_States_Pos : MonoBehaviour
{

    //this was an experimental script seeing what I could do with layer changes

    public AI_Position AI_Pos;
    private static float currentX, currentY, currentZ;

    // Start is called before the first frame update
    void Start()
    {
        AI_Pos.GetComponent<AI_Position>();
    }

    // Update is called once per frame
    void Update()
    {
        //stores the current position
        currentX = AI_Pos.xPos;
        currentY = AI_Pos.yPos;
        currentZ = AI_Pos.zPos;

        //gameObject.transform.Translate(currentX, currentY, currentZ);

        //updates the game objects position
        gameObject.transform.position =  new Vector3(currentX, currentY, currentZ);

        //if the AI isnt in the same position as the currentX,Y or Z positions its layer is -1
        if(gameObject.transform.position.x != currentX &&
            gameObject.transform.position.y != currentY &&
            gameObject.transform.position.z != currentZ)
        {
            gameObject.layer = -1;
        }

    }
}
