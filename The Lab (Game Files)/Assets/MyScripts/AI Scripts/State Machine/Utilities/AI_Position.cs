using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Position : MonoBehaviour
{

    //this script is used to store the position of the AI & helps parent projectile from despawning 

    public GameObject PathfindingAI; //stores the AI 
    public float xPos, yPos, zPos; //stores the AI position
    
    // Start is called before the first frame update
    void Start()
    {
        //if gameObject == Null each position = 0
        if(PathfindingAI == null)
        {
            xPos = 0;
            yPos = 0;
            zPos = 0;
        }

        //stores the X & Y positions
        xPos = PathfindingAI.transform.position.x;
        yPos = PathfindingAI.transform.position.y;

    }

    // Update is called once per frame
    void Update()
    {
        //stores the X & Y positions
        xPos = PathfindingAI.transform.position.x;
        yPos = PathfindingAI.transform.position.y;
    }
}
