using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Pathfinding_enabler : MonoBehaviour
{

    //this script is to enable/disable A* pathfinding for the idle state

    public bool enablePathfinding; //enables pathfinding
    private IdleState idle; //idle state
    public AIPath aIPath; //uses A* pathfindings AIPath script

    void Start()
    {
        //allows access to the variables of the AIPath script
        aIPath.GetComponent<AIPath>();

        //if enablePathfinding = false the AI cant move
        if (!enablePathfinding) aIPath.canMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        //if enablePathfinding = true the AI can move
        if (enablePathfinding) aIPath.canMove = true;

        //if enablePathfinding = false the AI cant move
        if (!enablePathfinding) aIPath.canMove = false;
    }
}
