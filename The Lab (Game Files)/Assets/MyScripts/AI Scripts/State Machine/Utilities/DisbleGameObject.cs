using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisbleGameObject : MonoBehaviour
{

    //this is a script used to test different states

    public GameObject idleGO; //idle state
    public GameObject chaseGO; //chase state
    public GameObject attackGO; //attack state
    public int DisableGO_val; //state value 

    // Update is called once per frame
    void Update()
    {
        //disables all states
        if (DisableGO_val == 0)
        {
            idleGO.SetActive(false);
            chaseGO.SetActive(false);
            attackGO.SetActive(false);
        }

        //enables idle state
        if (DisableGO_val == 1)
        {
            idleGO.SetActive(true);
            chaseGO.SetActive(false);
            attackGO.SetActive(false);
        }

        //enables chase state
        if (DisableGO_val == 2)
        {
            idleGO.SetActive(false);
            chaseGO.SetActive(true);
            attackGO.SetActive(false);
        }

        //enables attack state
        if (DisableGO_val == 3)
        {
            idleGO.SetActive(false);
            chaseGO.SetActive(false);
            attackGO.SetActive(true);
        }
    }
}
