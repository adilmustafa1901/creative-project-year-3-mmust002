using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatedState : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public HiddenState hiddenState; //used to switch to the hidden state
    private bool Deactivated; //detrmines if the AI is activated or not
    [SerializeField] private Animator TriggerAnimation; //triggers the activation animation 
    public Patroling_AI AI; //use to control the patrolling AIs script

    // Start is called before the first frame update
    void Start()
    {
        //allows me to use the script's variables
        hiddenState.GetComponent<Idle_Generator>();
        AI.GetComponent<Patroling_AI>();
    }

    // Update is called once per frame
    void Update()
    {
        //when activated deactivated = false
        if (hiddenState.Activated) Deactivated = false;
        else Deactivated = true;

        //controls the speed and animation when deactivated 
        if (Deactivated)
        {
            AI.speed = 0;
            TriggerAnimation.SetBool("Activate", false);
        }
        else //controls the speed and animation when activated 
        {
            AI.speed = 5;
            TriggerAnimation.SetBool("Activate", true);
        }
    }

    public override State RunCurrentState()
    {
        //if deactivated = false return the hidden state
        if (!Deactivated)
        {
            return hiddenState;
        }
        else 
        {
            return this;
        }
    }
}
