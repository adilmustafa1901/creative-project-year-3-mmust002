using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HiddenState : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public bool Activated; //used to switch state
    public ActivatedState activatedState; //used to switch to the activated state
    public GameObject KeyCard; //used to check the keycard used to activate the AI 
    public TextMeshPro text; //displays state


    //used to control the activate state
    public override State RunCurrentState()
    {
        //if activated the activated state is called
        if (Activated)
        {
            return activatedState;
        }
        else //otherwise call this state instead
        {
            return this;
        }
    }
    // Update is called once per frame
    void Update()
    {
        //if the keycard isnt gone activated = false
        if (KeyCard != null) Activated = false;
        else Activated = true; //if the keycard is gone activated = true

        if (Activated) text.text = "Activated";
        else if (!Activated) text.text = "Hidden";

    }
}
