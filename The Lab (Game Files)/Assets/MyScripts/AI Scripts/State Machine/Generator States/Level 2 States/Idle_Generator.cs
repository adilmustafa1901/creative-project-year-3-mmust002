using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Idle_Generator : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{

    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s


    //this script controls the idle state for the generators in level 2

    public bool PlayerIsInRange; //is true if player is in range
    public Attack_Generator attackGen; //used to switch to the Attack_Generator state/script
    public AI_Projectile projectile; //used to control the projectile script
    public Transform AI, player; //used to detect the distance between the AI and the player
    public float Dist, customRange = 10f; //distance related variables
    public TextMeshPro text; //displays state

    //used to control when to switch state
    public override State RunCurrentState()
    {
        //if the player is within range switch the the attack state
        if (PlayerIsInRange)
        {
            return attackGen;
        }
        else //if the player is not in range run the idle state
        {
            return this;
        }
    }
    void Start()
    {
        //used so I can use/alter the veriables from those scripts
        projectile.GetComponent<AI_Projectile>();
        AI.GetComponent<AI_Projectile>().enabled = false;
    }

    void Update()
    {
        //if the player is not in range dont shoot
        if (!PlayerIsInRange)
        {
            AI.GetComponent<AI_Projectile>().enabled = false;
            text.text = "Idle";
        }

        //Dist stores the distance between the player and the AI 
        if (player)
        {
            Dist = Vector2.Distance(player.position, AI.position);
        }

        //if Dist is less than the custom range PlayerIsInRange = true
        if(Dist < customRange)
        {
            PlayerIsInRange = true;
        }

        //if Dist is more than the custom range PlayerIsInRange = false
        if (Dist > customRange)
        {
            PlayerIsInRange = false;
        }

        //displays Idle in education mode
        if (PlayerIsInRange)
        {
            text.text = "Attack";
        }
    }
}
