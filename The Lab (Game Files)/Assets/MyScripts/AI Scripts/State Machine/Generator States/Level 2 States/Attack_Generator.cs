using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack_Generator : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public bool InRange; //checks if player is in range
    public Idle_Generator idleGen; //used to switch to the idle state
    public GameObject AI; //controls the AI GameObject

    // Start is called before the first frame update
    void Start()
    {
        //gets access to the idle state script
        idleGen.GetComponent<Idle_Generator>();
    }

    // Update is called once per frame
    void Update()
    {
        //uses the range bool from the idle state
        InRange = idleGen.PlayerIsInRange;

        //if the player is in range the AI can shoot projectiles
        if(InRange) AI.GetComponent<AI_Projectile>().enabled = true;
    }

    //used to control when to switch states
    public override State RunCurrentState()
    {
        //if the player is not within range switch to the idle state
        if (!InRange)
        {
            return idleGen;
        }
        else
        {
            return this;
        }
    }

 
}
