using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using TMPro;


public class TrackPlayer : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public bool nextToPlayer; //is true when near the player
    public AIPath aIPath; //used to alter the A* Pathfinding AIPath script
    public Transform AI_Pos, target; //used to calculate distance
    public float Dist, ChargeAttackRange = 10f; //stores distance
    public ChargeProjectile chargeProjectile; //used to switch to the ChargeProjectile state
    public TextMeshPro text; //displays state


    //used to change state
    public override State RunCurrentState()
    {
        //if next to player switch to the ChargeProjectile State
        if (nextToPlayer)
        {
            return chargeProjectile;
        }
        else 
        {
            return this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //used to control the variables within these scripts
        aIPath.GetComponent<AIPath>();
        AI_Pos.GetComponent<AI_Position>();
       
    }

    // Update is called once per frame
    void Update()
    {
        //calculates the distance between the player and the AI 
        if (target)
        {
            Dist = Vector2.Distance(target.position, AI_Pos.position);
            //print("Distance to other: " + dist);
        }

        //if the player is less than the custom range change its speed and nextToPlayer = true
        if(Dist < ChargeAttackRange)
        {
            aIPath.maxSpeed = 10;
            nextToPlayer = true;
        }

        //if the player is more than the custom range change its speed and nextToPlayer = false
        if (Dist > ChargeAttackRange)
        {
            aIPath.maxSpeed = 12;
            nextToPlayer = false;
        }

        //state text for education mode
        if (nextToPlayer)
        {
            text.text = "Charge Projectile";
        }
        else if (!nextToPlayer)
        {
            text.text = "Track Player";
        }
    }
}
