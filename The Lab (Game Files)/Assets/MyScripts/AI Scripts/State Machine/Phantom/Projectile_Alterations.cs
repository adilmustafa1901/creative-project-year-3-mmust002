using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Alterations : MonoBehaviour
{

    //This script is used to alter the player when the phantom projectile collidee with them 

    public TriggerAlterations triggerAlterations; //uses TriggerAlterations script to help alter the player
    public int selectEffect; //this script is applied to 3 different projectiles, this provides them with their effect
    private bool flip, speed, size; //each bool provides the effect

    // Start is called before the first frame update
    void Start()
    {
        //used to gain access to the TriggerAlterations variables
        triggerAlterations.GetComponent<TriggerAlterations>();
    }

    // Update is called once per frame
    void Update()
    {
        //if flip is selected flipcontrols will = true
        if(flip)
        triggerAlterations.FlipControls = true;

        //if speed is selected affectspeed will = true
        if (speed)
        triggerAlterations.AffectSpeed = true;

        //if size is selected changesize will = true
        if (size)
        triggerAlterations.ChangeSize = true;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerT"))
        {
            //this effect flips the players controls for a short period of time & destroys the projectile
            if (selectEffect == 0)
            {
                triggerAlterations.FlipControls = true;
                triggerAlterations.FlippedControlsTimer = triggerAlterations.flipControlsTime;
                Destroy(gameObject);
            }

            //this effect slows the player down for a short period of time & destroys the projectile
            if (selectEffect == 1)
            {
                triggerAlterations.AffectSpeed = true;
                triggerAlterations.AffectSpeedTimer = triggerAlterations.affectSpeedTime;
                Destroy(gameObject);
            }

            //this effect increases the player's size for a short period of time & destroys the projectile
            if (selectEffect == 2)
            {
                triggerAlterations.ChangeSize = true;
                triggerAlterations.ChangeSizeTimer = triggerAlterations.changeSizeTime;
                Destroy(gameObject);
            }
        }
    }
}
