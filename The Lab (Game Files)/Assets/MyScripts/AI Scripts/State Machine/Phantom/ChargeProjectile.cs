using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeProjectile : State //inherits from the State script (this allows the state manager script to switch between states/scripts)
{
    //this tutorial demonstrated how I can structure my state machine scripts https://www.youtube.com/watch?v=cnpJtheBLLY&t=1s

    public TrackPlayer trackPlayer; //uses TrackPlayer State
    public PhantomProjectile phantomProjectile; //uses PhantomProjectile script
    [SerializeField] private Animator TriggerAnimation1; //used to trigger the phantom charge animation 

    //used to change state
    public override State RunCurrentState()
    {
        //if the phantom is near the player return this
        if (trackPlayer.nextToPlayer)
        {
            return this;
        }
        //if the phantom isnt near the player return the trackPlayer state
        else if (!trackPlayer.nextToPlayer)
        {
            return trackPlayer;
        }
        else
        {
            return this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //used to control the variables in these scripts
        phantomProjectile.GetComponent<PhantomProjectile>();
    }

    // Update is called once per frame
    void Update()
    {
        //if the phantom is near the player enable its animation and enable projectiles
        if (trackPlayer.nextToPlayer)
        {
            TriggerAnimation1.SetBool("charge", true);
            phantomProjectile.Enable = true;
            

        }

        //if the phantom is not near the player disable its animation and disable projectiles
        if (!trackPlayer.nextToPlayer)
        {
            TriggerAnimation1.SetBool("charge", false);
            phantomProjectile.Enable = false;
        }
    }


}
