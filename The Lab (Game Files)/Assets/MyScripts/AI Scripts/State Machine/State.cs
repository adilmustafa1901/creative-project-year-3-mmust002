using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this is the base class, used to classify current state, this is done via inheritance
public abstract class State : MonoBehaviour
{
    public abstract State RunCurrentState();
}
