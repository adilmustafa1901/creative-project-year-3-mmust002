using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    //stores the current state
    public State currentState;

    // Update is called once per frame
    void Update()
    {
        RunStateMachine();
    }

    public void RunStateMachine()
    {
        //checks the next state
        State nextState = currentState?.RunCurrentState();

        if(nextState != null)
        {
            //swtich to next state
            SwitchToNextState(nextState);
        }
    }

    //allows the state manager to switch states
    private void SwitchToNextState(State nextState)
    {
        currentState = nextState;
    }
}
