using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
public class FSM_AI : MonoBehaviour
{

    //This Script was an attempt at creating a finite state machine using this tutorial https://www.youtube.com/watch?v=db0KWYaWfeM
    //I stopped midway as I kept getting errors

    private Vector3 startingPosition;
    private Vector3 roamPosition;
    private AI_Features pathfindingMovement;




    private void Awake()
    {
        pathfindingMovement = GetComponent<AI_Features>();
    }


    // Start is called before the first frame update
    private void Start()
    {
        startingPosition = transform.position;
        roamPosition = GetRoamingPosition();
    }

    // Update is called once per frame
    void Update()
    {
        //pathfindingMovement.target(roamPosition);
    }

    //makes AI have a random roaming position
    private Vector3 GetRoamingPosition()
    {
        return startingPosition + GetRandomDir() * Random.Range(10f, 70f);
    }

    //provides AI with a random direction
    public static Vector3 GetRandomDir()
    {
        return new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)).normalized;
    }

    //makes AI move to set target e.g player
    public void MoveTo(Vector3 targetPosition)
    {
        //SetTarget
    }
}
