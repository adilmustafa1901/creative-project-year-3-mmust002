using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AnimationTrigger : MonoBehaviour
{

    //this script is used to trigger all animations within each level

    [SerializeField] private Animator TriggerAnimation1; //first animation 
    [SerializeField] private Animator TriggerAnimation2; //second animation
    public TextMeshProUGUI KeyCardFoundText; //checks the objective

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player enters the trigger and the objective is to escape
        if (collision.CompareTag("PlayerT") && KeyCardFoundText.text == "Objective: Escape!")
        {

            //tutorial trigger
            TriggerAnimation1.SetBool("TutInfoExit", true);

            //lvl 1 triggers
            TriggerAnimation1.SetBool("OpenDoor", true);
            TriggerAnimation2.SetBool("OpenDoor2", true);
            TriggerAnimation1.SetBool("ExitTut", true);
            TriggerAnimation1.SetBool("DestroyWall", true);

            //lvl 2 triggers
            TriggerAnimation1.SetBool("OpenExitDoor2", true);
            TriggerAnimation2.SetBool("ExitLvl2", true);
        }

        //if the player enters with the trigger
        if (collision.CompareTag("PlayerT"))
        {

            //Tutorial Info Trigger
            TriggerAnimation1.SetBool("TutInfoEnter", true);
            TriggerAnimation1.SetBool("TutInfoEnter2", true);
            TriggerAnimation1.SetBool("TutInfoFade", true);
            TriggerAnimation2.SetBool("TutInfoTxt", true);

            //lvl 1 triggers
            TriggerAnimation1.SetBool("EnableFade", true);
            TriggerAnimation2.SetBool("EnableTextFade", true);

            //lvl 2 triggers
            TriggerAnimation1.SetBool("OpenDoor1", true);
            TriggerAnimation1.SetBool("OpenKeyCardWall", true);
            TriggerAnimation1.SetBool("NextLevel", true);

            //lvl3 triggers
            TriggerAnimation1.SetBool("Lvl3Enter", true);
            TriggerAnimation1.SetBool("Trap1", true);

            TriggerAnimation1.SetBool("Trap2", true);
            TriggerAnimation2.SetBool("Trap2", true);

            TriggerAnimation1.SetBool("CD3", true);
            TriggerAnimation1.SetBool("CD4", true);

            TriggerAnimation1.SetBool("Alarm", true);
            TriggerAnimation2.SetBool("HazardDoor", true);

            TriggerAnimation1.SetBool("ExitLevel", true);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if the player exits and trigger and the objective is to escape
        if (collision.CompareTag("PlayerT") && KeyCardFoundText.text == "Objective: Escape!")
        {

            //tutorial trigger
            TriggerAnimation1.SetBool("TutInfoExit", false);
            TriggerAnimation1.SetBool("ExitTut", false);

            //lvl 1 triggers 
            TriggerAnimation1.SetBool("OpenDoor", false);
            TriggerAnimation2.SetBool("OpenDoor2", false);


            //lvl 2 triggers 
            TriggerAnimation1.SetBool("OpenExitDoor2", false);



            
        }

        //if the player exits the trigger
        if (collision.CompareTag("PlayerT"))
        {

            //Tutorial Info Trigger
            TriggerAnimation1.SetBool("TutInfoEnter", false);
            TriggerAnimation1.SetBool("TutInfoEnter2", false);

            //lvl 2 triggers 
            TriggerAnimation1.SetBool("OpenDoor1", false);

            TriggerAnimation1.SetBool("ExitLevel", false);

            //lvl3 triggers
            //TriggerAnimation1.SetBool("Lvl3Enter", false);
        }
    }
}
