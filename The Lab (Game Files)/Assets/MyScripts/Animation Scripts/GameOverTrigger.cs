using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOverTrigger : MonoBehaviour
{
   
    [SerializeField] private Animator GameOverAnimation;  //gameover animation
    [SerializeField] private GameObject player;  //Player Game Object 
    public TextMeshProUGUI health, GameOverText, GameOverText2, EndText2; //UI text 
    
    // Start is called before the first frame update
    void Start()
    {
        //if the player doesnt exist play the game over animation
        if (player == null)
        {
            GameOverAnimation.SetBool("PlayerHasDied", true);
            GameOverAnimation.SetBool("PlayerHasDiedTut", true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if the player doesnt exist play the game over animation
        if (player == null)
        {
            GameOverAnimation.SetBool("PlayerHasDied", true);
            GameOverAnimation.SetBool("PlayerHasDiedTut", true);
            GameOverText.text = "Game Over!";
            GameOverText2.text = "Press R to restart";
            EndText2.text = "......";
        }

        //if the player doesnt exist R restarts the level 
        if (Input.GetKey(KeyCode.R) && EndText2.text == "......")
        {
            //SceneManager.LoadScene(4);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Debug.Log("R");
        }
    }
}
