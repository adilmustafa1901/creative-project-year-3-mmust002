using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title_Screen : MonoBehaviour
{
    void Update()
    {
        //takes user to the main menu
        StartGame();
    }

    public void StartGame() 
    {
        //if the user presses Space it will load the next scene (the main menu)
        if (Input.GetKey(KeyCode.Space)){
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            Debug.Log("SPACE");
        }
    }
}
