using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public static bool Paused; //checks if paused
    public GameObject UI, PauseBtn, SettingsUI; //UI game objects
    public int SceneNum; //used for the level select scene 

    // Update is called once per frame
    void Update()
    {
        //if the user presses P it pauses the game
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (Paused) //if paused resume
            {
                ResumeGame();
            }
            else //if not paused, pause the game
            {
                PauseGame();
            }
        }
    }

    //when called it resumes the game
    public void ResumeGame()
    {
        Paused = false;
        UI.SetActive(false);
        Time.timeScale = 1f;
        PauseBtn.SetActive(true);

    }

    //when called it pauses the game
    public void PauseGame()
    {
        Paused = true;
        UI.SetActive(true);
        Time.timeScale = 0f;
        SettingsUI.SetActive(false);
        PauseBtn.SetActive(false);
    }

    //when called settings is displayed
    public void Settings()
    {
        SettingsUI.SetActive(true);
        UI.SetActive(false);
        Debug.Log("Loading Settings");

    }

    //when called the level select scene is loaded
    public void LevelSelect()
    {
        Debug.Log("Loading Level Select");
        Time.timeScale = 1f;
        Paused = false;
        SceneManager.LoadScene(4);

    }

    //when called the main menu scene is loaded
    public void MainMenu()
    {
        Debug.Log("Loading Main Menu");
        Time.timeScale = 1f;
        Paused = false;
        SceneManager.LoadScene(1);

    }

    //when called it loads the level selected
    public void LevelSeleectBtn()
    {
        SceneManager.LoadScene(SceneNum);
    }

    //if the player clicks on the level, it loads its scene
    void OnMouseDown()
    {
        if (SceneNum != 0)
        {
            SceneManager.LoadScene(SceneNum);
        }
    }

}
