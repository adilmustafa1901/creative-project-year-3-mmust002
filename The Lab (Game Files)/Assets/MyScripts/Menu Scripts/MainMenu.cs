using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject MainMenuUI, CreditsUI, MainMenuBackground, CreditsBackground;
    //used with the play button
    public void Level1()  
    {
        //loads the next scene (level 2)
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }

    //used with the settings button 
    public void Settings()  
    {
        //loads the settings scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    //displays the credits UI
    public void Credits()
    {
        MainMenuUI.SetActive(false);
        MainMenuBackground.SetActive(false);

        CreditsUI.SetActive(true);
        CreditsBackground.SetActive(true);
    }

    //Displays the main menu UI
    public void Back()
    {
        MainMenuUI.SetActive(true);
        MainMenuBackground.SetActive(true);

        CreditsUI.SetActive(false);
        CreditsBackground.SetActive(false);
    }

    //used with the quit button
    public void Quit()
    {
        //allows me to know if the button works
        Debug.Log("Exiting Game"); 
        //quits the application
        Application.Quit(); 
    }
}
