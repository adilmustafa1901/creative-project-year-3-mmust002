using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class Settings : MonoBehaviour
{
    //this tutorial provided the information on how to create volume sliders https://www.youtube.com/watch?v=MmWLK9sN3s8
    public AudioMixer audioMixer;
    
    public void Exit()
    {
        //returns the user to the main menu
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    //master volume slider
    public void Volume(float Vol)
    {
        audioMixer.SetFloat("MasterVol", Mathf.Log10(Vol) * 20);
    }

    //music volume slider
    public void MusicVolume(float Vol)
    {
        audioMixer.SetFloat("MusicVol", Mathf.Log10(Vol) * 20);
    }

    //sfx volume slider
    public void SFXVolume(float Vol)
    {
        audioMixer.SetFloat("SFXVol", Mathf.Log10(Vol) * 20);
    }
}
