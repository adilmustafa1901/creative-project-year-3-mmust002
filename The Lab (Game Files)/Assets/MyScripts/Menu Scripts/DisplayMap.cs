using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayMap : MonoBehaviour
{
    //this script is for the map screen 

    public GameObject MapCamera, PlayerCamera, MapBtn, PlayBtn, PauseBtn; //cameras & UI elements

    //when the player clicks on the map button it displays the map 
    public void Map()
    {
        MapCamera.SetActive(true); //switches to the map cam 
        PlayerCamera.SetActive(false); // player camera is disabled
        PlayBtn.SetActive(true); //the play button is visible
        MapBtn.SetActive(false); //map button is disabled
        PauseBtn.SetActive(false); //pause button is disabled
        Time.timeScale = 0f; //the game is frozen
    }

    //when the player clicks the play button it resumes the game
    public void Resume()
    {
        MapCamera.SetActive(false); //map camera is disabled
        PlayerCamera.SetActive(true); //player camera is enabled
        PlayBtn.SetActive(false); //play button is disabled
        MapBtn.SetActive(true); //map button is enabled
        PauseBtn.SetActive(true); //pause button is enabled
        Time.timeScale = 1f; //the game is no longer frozen
    }
}
