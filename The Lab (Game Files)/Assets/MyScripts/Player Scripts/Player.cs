using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    public int TotalScore; //stores the total score
    public TextMeshProUGUI Score, KeyCardFoundText, EndText, EndText2, Health; //UI elements

    public int maxHealth = 255; //provides the max health of the player
    public int currentHealth; //stores the current health of the player

    void Start()
    {
        //UI at the beginning of the game
        Score.text = "Score: " + TotalScore + currentHealth;
        KeyCardFoundText.text = "Objective: Find The Key Card!";
        currentHealth = maxHealth;
        Health.text = "Health: " + currentHealth;
    }

    void Update()
    {
        //destroys the player game object when currentHealth is less than 0
        if(currentHealth < 0) Destroy(gameObject);

        Health.text = "Health: " + currentHealth; //health UI text

        //makes sure the health doesnt go over 255
        if (currentHealth > maxHealth)
        {
            currentHealth = 255;
        }
    }

    //used by other scripts to deal damage to the player 
    public void DamageManager(int damageValue)
    {
        currentHealth = currentHealth - damageValue;
    }

    //used by health increase scripts to add health to the player 
    public void HealthManager(int HealthValue)
    {
        if (currentHealth < maxHealth)
        {
            currentHealth = currentHealth + HealthValue;
        }
    }

    //this function is going to be used to determine what each collectable's value is via the PickUps Script
    public void CollectablesManager(int value)
    {
        TotalScore = TotalScore + value;
        Score.text = "Score: " + TotalScore;

    }

    //changes the objective when any keycard is collected
    public void KeyCardManager(int value, bool KeyCardCollected)
    {
        TotalScore = TotalScore + value;
        Score.text = "Score: " + TotalScore;

        if (!KeyCardCollected)
            KeyCardFoundText.text = "Objective: Find The Key Card!";

        if (KeyCardCollected)
            KeyCardFoundText.text = "Objective: Escape!";
    }

    //the tutorial end screen text 
    public void EndTutorial(int value)
    {
        TotalScore = TotalScore + value;
        Score.text = "";
        KeyCardFoundText.text = "";
        EndText.text = "You Completed the Tutorial!" + 
                       " Your score is: " + TotalScore + 
                       "                    Press Enter to Continue ";
        EndText2.text = ".";


    }

    //the level complete end screen text 
    public void EndLevel(int value)
    {
        TotalScore = TotalScore + value;
        Score.text = "";
        KeyCardFoundText.text = "";
        EndText.text = "You Completed the Level!" +
                       "       Your score is: " + TotalScore +
                       "                    Press Enter to Continue ";
        EndText2.text = ".";


    }

    //returns current health 
    public int retCurrentHealth()
    {
        return currentHealth;
    }
}
