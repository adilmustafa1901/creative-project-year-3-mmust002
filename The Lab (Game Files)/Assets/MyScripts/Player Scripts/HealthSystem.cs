using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HealthSystem : MonoBehaviour
{

    //script is used to deal damage to the player e.g projectile colliding 

    //public float maxHealth = 255f;
    //public float currentHealth = 255f;
    public int damage = 1; //damage dealt
    private int HealthIncrease; //health increase
    public float reduceAlpha; //attempt to reduce alpha of the animated floor
    private float playerHealth; //stores players health 

    //public bool healthPack = false;

    private bool takingDamage = false; //when true player is taking damage
    private Player player; //gains access to the player script

    void Start()
    {
        //gains access to the player scripts variables
        player = FindObjectOfType<Player>();
    }

    void Update()
    {
        //used for testing purposes
        //HealthIncrease = -damage;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerT"))
        {
            //if the projectile enters the player it deals damage
            player.DamageManager(damage);

            //if(!healthPack) player.healthManager(damage);
            //if (healthPack) player.healthManager(HealthIncrease);

            Debug.Log(playerHealth);
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        //if the collision stays in the player it still deals damage (used for the red floor in level 3)
        if (collision.CompareTag("PlayerT"))
        {
            player.DamageManager(damage);
            Debug.Log(playerHealth);
        }
    }

    //testing what happens when the collision exits the player
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerT"))
        {
            //takingDamage = false;
            Debug.Log(playerHealth);
        }
    }

    

  

}
