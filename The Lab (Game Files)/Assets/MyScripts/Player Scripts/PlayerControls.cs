using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerControls : MonoBehaviour
{
    public float Player_Speed = 5f; //controls the speed of the player
    public Rigidbody2D rb; //allows me to interact with rigidbody2D
    public Vector2 movement; //used to implement movement
    public int floorNum;
    public TextMeshProUGUI EndText2;

    public bool IsRed, IsGreen, IsYellow, IsRainbow, IsNormal, flipControls, SwitchControls, ChangeSize,SideScroller, normalControls = true;

    public int mode; //turns off/on AI/education mode

    // Update is called once per frame meaning its used for movement
    void Update()
    {
        if (normalControls)
        {
            //allows movement with the x axis / horizontal (also lets user us A,D Left Arrow & Right Arrow)
            movement.x = Input.GetAxisRaw("Horizontal");

            //allows movement with the y axis / vertical (also lets user us W,S Up Arrow & Down Arrow)
            movement.y = Input.GetAxisRaw("Vertical");
            //flipControls = false;
            //SwitchControls = false;
        }

        //*
        if (flipControls)
        {
            //allows movement with the x axis / horizontal (also lets user us A,D Left Arrow & Right Arrow)
            movement.x = -Input.GetAxisRaw("Horizontal");

            //allows movement with the y axis / vertical (also lets user us W,S Up Arrow & Down Arrow)
            movement.y = -Input.GetAxisRaw("Vertical");

            //normalControls = false;
            //SwitchControls = false;
        }
        
        //used for the level select screen
        if (SideScroller)
        {
            movement.x = Input.GetAxisRaw("Horizontal");
        }

        //provides each floor type with different speeds
        if (IsRed)
        {
            Player_Speed = 5f;
        }
        else if (IsGreen)
        {
            Player_Speed = 12f;
        }
        else if (IsYellow)
        {
            Player_Speed = 8f;
        }
        else if (IsRainbow)
        {
            Player_Speed = 20f;
        }
        else if (IsNormal)
        {
            Player_Speed = 10f;
        }

        NextLevel();
        QuitGame();
        RestartLevel();
        //Lvl3();
        ChangeMode();

        mode = mode % 2;
    }
    
    //isnt linked to framerate meaning it will handle the physics
    void FixedUpdate()
    {
        //moves the position and doesnt link it to the users framerate
        rb.MovePosition(rb.position + movement * Player_Speed * Time.fixedDeltaTime);
    }

    //used for users who played previous levels
    public void Lvl3()
    {
        if(Input.GetKey(KeyCode.Alpha0))
        SceneManager.LoadScene(6);
    }
    
    public void NextLevel()
    {
        if (Input.GetKey(KeyCode.Return) && EndText2.text == ".")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            EndText2.text = "";
            //SceneManager.LoadScene(4);
        }
        
    }

    //if the player presses escape the game will close (useful for the downloaded version)
    public void QuitGame()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

    }

    //when the player presses R it reloads the scene
    public void RestartLevel()
    {
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }

    //switches between normal and educational mode
    public void ChangeMode()
    {
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            mode++;
        }
    }


}
