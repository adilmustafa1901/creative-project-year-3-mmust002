using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCam : MonoBehaviour
{
    //lets me use the transform feature 
    private Transform playerPositioning;

    public float camOffSetX, camOffSetY;

    // Start is called before the first frame update
    void Start()
    {
        //links playerPositioning with the PlayerT tag ive created in unity 
        playerPositioning = GameObject.FindGameObjectWithTag("PlayerT").transform;
    }


    void LateUpdate()
    {
        //stores the cameras x/y position temporarlily (if Vector2 doesnt render background)
        Vector3 camTemp = transform.position;

        //links the temp position to the current player's x/y position 
        camTemp.x = playerPositioning.position.x;
        camTemp.y = playerPositioning.position.y;

        //if I have modified the camOffSet variable it applies it to the camTemp variable
        camTemp.x += camOffSetX;
        camTemp.y += camOffSetY;

        //links the current position to the temp camera position (this is because you cannot modify transform.position.x)
        transform.position = camTemp;
    }
}
