using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAlterations : MonoBehaviour
{

    //this script is used to trigger the player alterations and gives them a timer

    public bool FlipControls, AffectSpeed, ChangeSize, IsLevel3; //triggers the effects 
    public PlayerControls playerControls; //gain access to the PlayerControls script
    public int FlippedControlsTimer, AffectSpeedTimer, ChangeSizeTimer; //the timer for each effect 
    public int flipControlsTime = 500, affectSpeedTime = 500, changeSizeTime = 500; //used by other scripts
    
    public Transform player; //used to change the players size

    private float scale = 0.5f; //the original size of the player

    // Start is called before the first frame update
    void Start()
    {
        //gains access to the playerControls variables
        playerControls = FindObjectOfType<PlayerControls>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //timer decreases if over 0
        if(FlippedControlsTimer > 0)
        {
            FlippedControlsTimer--;
        }
        //if the timer doesnt go less than 0 & makes flipControls false
        if(FlippedControlsTimer <= 0)
        {
            FlippedControlsTimer = 0;
            FlipControls = false;
        }
        //flips the players controls
        if (FlipControls)
        {
            playerControls.flipControls = true;
        }
        //sets the controls to normal
        if (!FlipControls)
        {
            playerControls.flipControls = false;
        }

        //-------------------------------------------------------------

        //timer decreases if over 0
        if (AffectSpeedTimer > 0)
        {
            AffectSpeedTimer--;
        }
        //if the timer doesnt go less than 0 & makes flipControls false
        if (AffectSpeedTimer <= 0)
        {
            AffectSpeedTimer = 0;
            AffectSpeed = false;
        }
        //slows the player
        if (AffectSpeed)
        {
            playerControls.Player_Speed = 5;
        }
        //makes their speed normal
        if (!AffectSpeed && !IsLevel3)
        {
            playerControls.Player_Speed = 10;
        }

        //-------------------------------------------------------------

        //timer decreases if over 0
        if (ChangeSizeTimer > 0)
        {
            ChangeSizeTimer--;
        }
        //if the timer doesnt go less than 0 & makes flipControls false
        if (ChangeSizeTimer <= 0)
        {
            ChangeSizeTimer = 0;
            ChangeSize = false;
        }
        //increases the players size
        if (ChangeSize)
        {
            scale += 0.005f;
            player.localScale = new Vector2(scale, scale);
        }
        //their size becomes normal
        if (!ChangeSize)
        {
            scale = 0.5f;
            player.localScale = new Vector2(0.5f, 0.5f);
        }
    }
}
