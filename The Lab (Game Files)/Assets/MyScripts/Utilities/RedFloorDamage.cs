using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedFloorDamage : MonoBehaviour
{
    //this script is to damage the player when they are on the red floor 

    public HealthSystem health; //access to HealthSystem script
    public PlayerControls playerControls; //access to PlayerControls script

    // Start is called before the first frame update
    void Start()
    {
        //access to script variables
        health.GetComponent<HealthSystem>();
        playerControls.GetComponent<PlayerControls>();
    }

    // Update is called once per frame
    void Update()
    {
        //if the player isnt on the red floor, take 0 damage
        if(playerControls.IsYellow == true || playerControls.IsGreen == true || playerControls.IsRainbow == true)
        {
            health.damage = 0;
        }
        //if they are on the red floor take 10 damage per frame
        else health.damage = 10;
    }
}
