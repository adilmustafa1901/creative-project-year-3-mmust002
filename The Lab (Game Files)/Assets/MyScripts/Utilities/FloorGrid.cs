using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorGrid : MonoBehaviour
{

    //"https://www.youtube.com/watch?v=u2_O-jQDD6s" was used to help create the script 
    
    //this script is to draw the square grid in the scene
    
    public int rows = 5; //used to alter the rows
    public int cols = 8; //used to alter the columns
    public float size = 1; //used to provide the space between each square
    public int VersionNum = 1; //used to choose what type of grid is required
    

    // Start is called before the first frame update
    void Start()
    {
        DrawFloorGrid(); //draws the grid immediatly 
    }

    private void DrawFloorGrid()
    {
        if (VersionNum == 1)
        {
            //selects the first floor prefab
            GameObject floorObjectTemp = (GameObject)Instantiate(Resources.Load("Prefabs/floorPre"));
        
            //loop places the prefab
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    ///the vid helped identify how to use game objects 
                    GameObject floorObject = (GameObject)Instantiate(floorObjectTemp,transform);

                    float posX = j * size;
                    float posY = i * -size;

                
                    floorObject.transform.position = new Vector2(posX, posY);
                }
            }
            //destroys the temporary floor object 
            Destroy(floorObjectTemp);

            //provides the grid width & height
            float gridWidth = cols * size,
                  gridHeight = rows * size;

            //positions the grid
            transform.position = new Vector2((-gridWidth / 2) + (size / 2),(gridHeight / 2) - (size / 2));
        }

        if (VersionNum == 2)
        {
            //selects the second floor prefab
            GameObject floorObjectTemp = (GameObject)Instantiate(Resources.Load("Prefabs/floorPre2"));

            //loop places the prefab
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    ///the vid helped identify how to use game objects 
                    GameObject floorObject = (GameObject)Instantiate(floorObjectTemp, transform);

                    float posX = j * size;
                    float posY = i * -size;


                    floorObject.transform.position = new Vector2(posX, posY);
                }
            }

            //destroys the temporary floor object 
            Destroy(floorObjectTemp);

            //provides the grid width & height
            float gridWidth = cols * size,
                  gridHeight = rows * size;

            //positions the grid
            transform.position = new Vector2((-gridWidth / 2) + (size / 2), (gridHeight / 2) - (size / 2));
        }

        if (VersionNum == 3)
        {
            //selects the second floor prefab
            GameObject floorObjectTemp = (GameObject)Instantiate(Resources.Load("Prefabs/floorPre3"));

            //loop places the prefab
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    ///the vid helped identify how to use game objects 
                    GameObject floorObject = (GameObject)Instantiate(floorObjectTemp, transform);

                    float posX = j * size;
                    float posY = i * -size;


                    floorObject.transform.position = new Vector2(posX, posY);
                }
            }

            //destroys the temporary floor object 
            Destroy(floorObjectTemp);

            //provides the grid width & height
            float gridWidth = cols * size,
                  gridHeight = rows * size;

            //positions the grid
            transform.position = new Vector2((-gridWidth / 2) + (size / 2), (gridHeight / 2) - (size / 2));
        }
    }

}
