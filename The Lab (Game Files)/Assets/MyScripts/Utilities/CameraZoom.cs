using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{

    //this script is used to allow users to zoom in or out 

    //this tutorial provided the code for this script https://www.youtube.com/watch?v=jmTUUP33GHs
    private Camera cam; //stores the camera
    private float targetZoom; //provides the zoom value
    private float zoomFactor = 3f; 
    [SerializeField] private float zoomLerpSpeed = 10; //provides the speed of the zoom 

    // Start is called before the first frame update
    void Start()
    {
        //selects the camera
        cam = Camera.main;
        //allows the camera to zoom
        targetZoom = cam.orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {
        //uses the mouse wheel to zoom 
        float scrollData = Input.GetAxis("Mouse ScrollWheel"); 

        //alters the value of targetZoom and alters the camera
        targetZoom -= scrollData * zoomFactor; 
        targetZoom = Mathf.Clamp(targetZoom, 4.5f, 10f);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetZoom, Time.deltaTime * zoomLerpSpeed);
    }
}
