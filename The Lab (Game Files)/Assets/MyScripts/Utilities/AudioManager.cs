using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class AudioManager : MonoBehaviour
{
    //this script is used to manage the sfx audio for the lab 

    public AudioSource[] audioSource; //stores the audio sources
    public int AudioNum; //used to select what segment of this script the audio file needs
    public TextMeshProUGUI Txt; //checks UI text 
    public Transform player, AI; //checks distance between player and AI 
    public float Dist, DistanceVal; //stores the distance 

    // Update is called once per frame
    void Update()
    {
        //checks the distance between the player and AI and alters its volume 
        if (player)
        {
            //calculates distance between the AI and player
            Dist = Vector2.Distance(player.position, AI.position);

            //sets the volume to 0.175
            if (Dist < DistanceVal - 1f && 
                Dist > (DistanceVal / 3) * 2)
            {
                audioSource[0].volume = 0.175f;
            }
            //sets the volume to 0.45
            else if (Dist < DistanceVal / 3)
            {
                audioSource[0].volume = 0.45f;
            }
            //sets the volume to 0.35
            else if (Dist < (DistanceVal / 3) * 2 && 
                Dist > DistanceVal / 3)
            {
                audioSource[0].volume = 0.35f;
            }
            //sets the volume to 0.07
            else if (Dist > DistanceVal - 1f && 
                Dist > (DistanceVal / 3)*2 && 
                Dist < DistanceVal && 
                Dist < DistanceVal - 0.5f)
            {
                audioSource[0].volume = 0.07f;
            }
            //sets the volume to 0.035
            else if (Dist < DistanceVal && 
                Dist > DistanceVal - 0.5f)
            {
                audioSource[0].volume = 0.035f;
            }
            //sets the volume to 0
            else
            {
                audioSource[0].volume = 0.0f;

            }
        }

        //used for projectiles created from the generators
        if(AudioNum == 2 && AI)
        {
            //sets the volume to 0.15
            if (Dist < DistanceVal)
            {
                audioSource[0].volume = 0.15f;
            }
            //sets the volume to 0
            else
            {
                audioSource[0].volume = 0.0f;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if true it will play the audio source
        if (collision.CompareTag("PlayerT") && AudioNum == 0)
        {
            audioSource[0].Play();
        }

        //this is used for the exit 
        if(AudioNum == 1)
        {
            //if the object is to escape it plays the first source
            if(collision.CompareTag("PlayerT") && Txt.text == "Objective: Escape!")
            {
                audioSource[0].Play();

            }
            //if the objective isnt to escape it will play the second audio source
            if (collision.CompareTag("PlayerT") && Txt.text != "Objective: Escape!")
            {
                audioSource[1].Play();
            }
        }
    }

    //this is used for projectiles
    private void OnTriggerExit2D(Collider2D collision)
    {
        //if it collides with the generator it will play the audio source
        if (AudioNum == 2 && collision.CompareTag("Generator"))
        {
            audioSource[0].Play();
        }
    }
}
