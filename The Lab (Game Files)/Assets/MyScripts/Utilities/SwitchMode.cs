using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchMode : MonoBehaviour
{
    
    public PlayerControls playerControls; //used to know what mode the game is in 
    public GameObject[] gameObjects; //used to store hidden game objects

    void Start()
    {
        //access to its variables
        playerControls.GetComponent<PlayerControls>();
    }

    void Update()
    {
        //game objects stay hidden 
        if(playerControls.mode == 0)
        {
            for(int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i].SetActive(false);
            }
        }

        //game objects are no longer hidden
        if (playerControls.mode == 1)
        {
            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i].SetActive(true);
            }
        }
    }
}
