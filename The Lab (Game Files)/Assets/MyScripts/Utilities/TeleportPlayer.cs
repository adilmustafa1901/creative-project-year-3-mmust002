using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlayer : MonoBehaviour
{
    //this script is to teleport the player in level 3 to the second segment 

    private Player player; //gains access to the player script
    public float NewX = -350, NewY = 7; //the position the player teleports to 
    public bool Teleport; //when true the player teleports
    // Start is called before the first frame update
    void Start()
    {
        //gains access to the player script variables
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        //teleports the player
        if(Teleport)
        player.transform.position = new Vector2(NewX, NewY);

        //when player is at the position teleport is false
        if (player.transform.position.x == NewX && player.transform.position.y == NewY)
            Teleport = false;

    }

    //when player collides with the game object, teleport = true
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerT"))
        {
            Teleport = true;
        }
    }


}
