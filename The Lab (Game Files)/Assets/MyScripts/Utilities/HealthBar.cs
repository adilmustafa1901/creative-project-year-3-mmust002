using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider; //used to increase/decrease the health bar
    public Player player; //used to get the current health 

    void Start()
    {
        //allows access to this scripts variables
        player.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        //slider is linked to the current health of the player
        slider.value = player.currentHealth;
    }
}
