using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecificQantityTrigger : MonoBehaviour
{
    //this script was made with the level 3 trap room puzzle in mind

    public GameObject[] gameObjects; //stores multiple game objects
    public GameObject outcomeGameObject; //the output game object 
    [SerializeField] private Animator TriggerAnimation1; //triggers the intended animation 
    public bool triggerAnimation, destroyGameobject, enableGameObject, checkOneIsNull; 
    public int nullCount; //checks how many gameobjects are null

    // Start is called before the first frame update
    void Start()
    {
        nullCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //checks if one is null
        if (checkOneIsNull)
        {
            for (int i = 0; i < gameObjects.Length; i++)
            {
                if (gameObjects[i] == null)
                {
                    //animation trigger
                    if (triggerAnimation)
                    {
                        TriggerAnimation1.SetBool(" ", true);
                    }

                    //destroys the outcome game object 
                    if (destroyGameobject)
                    {
                        Destroy(outcomeGameObject);
                    }

                    //enables the outcomeGameObject
                    if (enableGameObject)
                    {
                        outcomeGameObject.SetActive(true);
                    }
                }
            }
        }

        //used to check if all is null
        if (!checkOneIsNull)
        {
            //makes sure the null count doesnt go over the length of the gameObjects
            if (nullCount > gameObjects.Length)
            {
                nullCount = gameObjects.Length;
            }

            //makes sure the count doesnt go lower than 0
            if (nullCount < 0)
            {
                nullCount = 0;
            }

            //loops through the gameObjects array
            for (int i = 0; i < gameObjects.Length; i++)
            {
                //if an object is null +1
                if (gameObjects[i] == null)
                {
                    nullCount++;
                }
                //if an object isnt null -1
                if (gameObjects[i] != null)
                {
                    nullCount--;
                }
                //triggers the expected result 
                if (nullCount == gameObjects.Length * 2)
                {
                    if (triggerAnimation)
                    {
                        TriggerAnimation1.SetBool(" ", true);
                    }

                    if (destroyGameobject)
                    {
                        Destroy(outcomeGameObject);
                    }

                    if (enableGameObject)
                    {
                        outcomeGameObject.SetActive(true);
                    }
                }
            }
        }
    }
}   

