using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlterPlayerStats : MonoBehaviour
{

    private Player player; //Player script
    private PlayerControls playerControls; //PlayerControls script
    public float PlayerSpeed; //controls player speed
    public int floorVal; //provides a value to each floor type
    public bool Red, Green, Yellow, Rainbow, flip, SwitchC, ChangeSize, Normal; //used to detect what floor the player is on 
    public Rigidbody2D rb; //allows interaction with rigidbody2D


    // Start is called before the first frame update
    void Start()
    {
        //allows us to access the variables within these scripts
        player = FindObjectOfType<Player>();
        playerControls = FindObjectOfType<PlayerControls>();
    }

    // Update is called once per frame
    void Update()
    {
        //makes sure when "IsNormal" is true the others are false
        if (playerControls.IsNormal)
        {
            playerControls.IsNormal = true;
            playerControls.IsRainbow = false;
            playerControls.IsYellow = false;
            playerControls.IsGreen = false;
            playerControls.IsRed = false;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //makes sure when "IsRainbow" is true the others are false
        if (Rainbow)
        {
            playerControls.IsRainbow = true;
            playerControls.IsYellow = false;
            playerControls.IsGreen = false;
            playerControls.IsRed = false;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsYellow" is true the others are false
        if (Yellow)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = true;
            playerControls.IsGreen = false;
            playerControls.IsRed = false;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsGreen" is true the others are false
        if (Green)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = false;
            playerControls.IsGreen = true;
            playerControls.IsRed = false;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsRed" is true the others are false
        if (Red)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = false;
            playerControls.IsGreen = false;
            playerControls.IsRed = true;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsNormal" is true the others are false
        if (Normal)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = false;
            playerControls.IsGreen = false;
            playerControls.IsRed = false;
            playerControls.IsNormal = true;
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //makes sure when "IsRainbow" is true the others are false
        if (Rainbow)
        {
            playerControls.IsRainbow = true;
            playerControls.IsYellow = false;
            playerControls.IsGreen = false;
            playerControls.IsRed = false;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsYellow" is true the others are false
        if (Yellow)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = true;
            playerControls.IsGreen = false;
            playerControls.IsRed = false;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsGreen" is true the others are false
        if (Green)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = false;
            playerControls.IsGreen = true;
            playerControls.IsRed = false;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsRed" is true the others are false
        if (Red)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = false;
            playerControls.IsGreen = false;
            playerControls.IsRed = true;
            playerControls.IsNormal = false;
        }
        //makes sure when "IsNormal" is true the others are false
        if (Normal)
        {
            playerControls.IsRainbow = false;
            playerControls.IsYellow = false;
            playerControls.IsGreen = false;
            playerControls.IsRed = false;
            playerControls.IsNormal = true;
        }

    }
    //makes sure when "IsNormal" when the player exits the collider
    private void OnTriggerExit2D(Collider2D collision)
    {
        flip = false;
        SwitchC = false;

        playerControls.IsNormal = true;
        playerControls.IsRainbow = false;
        playerControls.IsYellow = false;
        playerControls.IsGreen = false;
        playerControls.IsRed = false;


    }
}
