using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SelectLevel : MonoBehaviour
{
    //this is the level select script

    public int SceneNum; //used to load each level
    private int SceneNumber; //used to update depending on the cursors location 
    public bool IsSelected, IsLevel; //used to select/load level 

    public TextMeshPro Enter; //on screen text that appears

    // Update is called once per frame
    void Update()
    {
        //if the cursor is on a level and the user presses space, it loads its scene
        if (IsSelected && Input.GetKey(KeyCode.Space))  SceneManager.LoadScene(SceneNumber); 
    }

    //checks if the player is on a level 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsLevel)
        {
            IsSelected = true;
            SceneNumber = SceneNum;
            Enter.text = "Press SPACE to Play";
        }
    }

    //checks if the player isnt on a level 
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (IsLevel)
        {
            IsSelected = false;
            Enter.text = " ";
        }
    }
}
