using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    //this script is used to make sure the music files do not get destroyed when loading a scene 

    public  MusicManager MusicManagerInstance; //calls this class
    private AudioSource audioSource; //used to store the music file

    void Awake()
    {
        //allows us to alter this component
        audioSource = GetComponent<AudioSource>();

        //doesnt destroy the music file
        DontDestroyOnLoad(this);

        if (MusicManagerInstance == null)
        {
            //MusicManagerInstance = this;
        }
        else //sets the game object false if the music manage instance exists
        {
            gameObject.SetActive(false);
        }
    }

    //this tutorial showed me how to create this script https://www.youtube.com/watch?v=_5ekbSrNcPU

}
