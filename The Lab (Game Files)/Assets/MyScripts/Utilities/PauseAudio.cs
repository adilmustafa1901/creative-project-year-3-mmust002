using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseAudio : MonoBehaviour
{

    //this script is to make sure the music files per level dont overlap eachother. 
    //This is to make sure when the player restarts the level the music doesnt get reset. 
    //also they dont get destroyed when going onto a new scene meaning they need to be paused. 

    private PauseAudio pa; //calls this class
    public int sceneNum; //provides the scene number
    void Awake()
    {

       // Scene currentScene = SceneManager.GetActiveScene();

        //links each object with a tag
        GameObject[] titleMusic = GameObject.FindGameObjectsWithTag("TitleMusic");
        GameObject[] tutMusic = GameObject.FindGameObjectsWithTag("TutMusic");
        GameObject[] lvl1Music = GameObject.FindGameObjectsWithTag("Lvl1Music");
        GameObject[] lvl2Music = GameObject.FindGameObjectsWithTag("Lvl2Music");
        GameObject[] lvl3Music = GameObject.FindGameObjectsWithTag("Lvl3Music");

        

        //pa.GetComponent<PauseAudio>();


        /*
        // Retrieve the index of the scene in the project's build settings.
        int buildIndex = currentScene.buildIndex;

        // Check the scene name as a conditional.
        switch (buildIndex)
        {
            case 3:
                titleMusic[0].gameObject.SetActive(false);
                tutMusic[0].gameObject.SetActive(true);
                lvl1Music[0].gameObject.SetActive(false);
                lvl2Music[0].gameObject.SetActive(false);
                lvl3Music[0].gameObject.SetActive(false);

                Destroy(tutMusic[1].gameObject);
                break;
            case 5:
                titleMusic[0].gameObject.SetActive(false);
                tutMusic[0].gameObject.SetActive(false);
                lvl1Music[0].gameObject.SetActive(true);
                lvl2Music[0].gameObject.SetActive(false);
                lvl3Music[0].gameObject.SetActive(false);

                Destroy(lvl1Music[1].gameObject);
                Destroy(tutMusic[0].gameObject);
                break;
        }
        */

        //in the scene number is 1 only play the tutorial music
        if (sceneNum == 1)
        {
            for (int i = 0; i < titleMusic.Length; i++)
            titleMusic[0].gameObject.SetActive(false);


            tutMusic[0].gameObject.SetActive(true);

            for (int i = 0; i < lvl1Music.Length; i++)
            lvl1Music[0].gameObject.SetActive(false);

            for (int i = 0; i < lvl2Music.Length; i++)
            lvl2Music[0].gameObject.SetActive(false);

            for (int i = 0; i < lvl3Music.Length; i++)
            lvl3Music[0].gameObject.SetActive(false);

            Destroy(tutMusic[1].gameObject);
                


        }

        //if the scene num == 2 only play the lvl1 music
        if (sceneNum == 2)
        {

            for(int i = 0; i<tutMusic.Length; i++)
            tutMusic[0].gameObject.SetActive(false);

            lvl1Music[0].gameObject.SetActive(true);

            for(int i = 0; i< lvl2Music.Length; i++)
            lvl2Music[0].gameObject.SetActive(false);

            for(int i = 0; i< lvl3Music.Length; i++)
            lvl3Music[0].gameObject.SetActive(false);

            Destroy(lvl1Music[1].gameObject);

                //for(int i = 0; i < tutMusic.Length; i++)
                //Destroy(tutMusic[i]);
            

        }

        //if scene num == 3 only play lvl2 music 
         if (sceneNum == 3)
        {

            for (int i = 0; i < titleMusic.Length; i++)
            titleMusic[0].gameObject.SetActive(false);

            for(int i = 0; i< tutMusic.Length; i++)
            tutMusic[0].gameObject.SetActive(false);

            for(int i = 0; i< lvl1Music.Length; i++)
            lvl1Music[0].gameObject.SetActive(false);

            lvl2Music[0].gameObject.SetActive(true);

            for(int i = 0; i< lvl3Music.Length; i++)
            lvl3Music[0].gameObject.SetActive(false);

            Destroy(lvl2Music[1].gameObject);

        }


         //if sceneNum == 4 only play level 3 music
         if (sceneNum == 4)
        {

            for(int i = 0; i< titleMusic.Length; i++)
            titleMusic[0].gameObject.SetActive(false);

            for(int i = 0; i< tutMusic.Length; i++)
            tutMusic[0].gameObject.SetActive(false);

            for(int i = 0; i< lvl1Music.Length; i++)
            lvl1Music[0].gameObject.SetActive(false);

            for(int i = 0; i< lvl2Music.Length; i++)
            lvl2Music[0].gameObject.SetActive(false);


            lvl3Music[0].gameObject.SetActive(true);

            Destroy(lvl3Music[1].gameObject);

        }
        


    }
}
