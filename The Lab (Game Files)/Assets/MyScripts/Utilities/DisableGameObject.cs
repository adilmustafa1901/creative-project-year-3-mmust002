using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableGameObject : MonoBehaviour
{

    //script is used to disable or enable game objects

    public GameObject[] gObject; //stores the game objects
    public bool enableGameObject = false; //when true the game objects selected will be enabled

    void OnTriggerEnter2D(Collider2D collision)
    {
        for (int i = 0; i < gObject.Length; i++)
        {
            //destroys the game objects
            if (collision.CompareTag("PlayerT") && !enableGameObject)
            {
                Destroy(gObject[i]);
            }

            //enables the game objects
            if (collision.CompareTag("PlayerT") && enableGameObject)
            {
                gObject[i].SetActive(true);

            }
        }

    }
}
