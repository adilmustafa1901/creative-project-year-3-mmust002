Welcome to my Git Repo!


/////////// Companion Website ///////////


- To locate the companion website files click on the "Companion Website" folder

The link below takes you to the companion website homepage
- https://www.doc.gold.ac.uk/~mmust002/TheLab/index.html




///////////// Game Files /////////////


- To view the scripts I've created click on "The Lab (Game Files)" 
- next, click on Assets
- next, click on the MyScripts folder 
you will now be able to see the scripts Ive implemented 


- my .gitignore uses what unity provides officially 
link: https://github.com/github/gitignore/blob/master/Unity.gitignore


- The game and be played/downloaded via the companion website, the link below takes you to the game page 
link: https://www.doc.gold.ac.uk/~mmust002/TheLab/HTML_Files/TheLabGame/index.html


////////// Supporting Documents /////////
If you would like to see my supporting documents click on the 'documents' folder




